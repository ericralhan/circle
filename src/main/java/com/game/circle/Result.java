package com.game.circle;

import java.util.List;

public class Result {

    private final Integer winner;
    private final List<Integer> eliminationSequence;

    public Result(Integer winner, List<Integer> eliminationSequence) {
        this.winner = winner;
        this.eliminationSequence = eliminationSequence;
    }

    public Integer getWinner() {
        return winner;
    }

    public List<Integer> getEliminationSequence() {
        return eliminationSequence;
    }

}
