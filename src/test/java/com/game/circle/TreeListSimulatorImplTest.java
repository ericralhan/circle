package com.game.circle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class TreeListSimulatorImplTest extends SimulatorTest {

    @BeforeEach
    public void init() {
        simulator = new TreeListSimulatorImpl();
    }

    // 21,474,836 Children with K = 1 runs around 22 seconds
    @Disabled
    @Test
    public void testLargeNumberStepOneCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 1);
        assertNotNull(result);
    }

    // 21,474,836 Children with K = 10 runs around 25 seconds
    @Disabled
    @Test
    public void testLargeNumberStepTenCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 10);
        assertNotNull(result);
    }

    // 21,474,836 Children with K = 100 runs around 35 seconds
    @Disabled
    @Test
    public void testLargeNumberStepHundredCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 100);
        assertNotNull(result);
    }

    // 21,474,836 Children with K = 1000 runs around 49
    @Disabled
    @Test
    public void testLargeNumberStepThousandCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 1000);
        assertNotNull(result);
    }

    // 21,474,836 Children with K = 10000 runs around 1m 4s
    @Disabled
    @Test
    public void testLargeNumberStepTenThousandCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 10000);
        assertNotNull(result);
    }

    // 21,474,836 Children with K = 100000 runs around 1m 1s
    @Disabled
    @Test
    public void testLargeNumberStepHundredThousandCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 100000);
        assertNotNull(result);
    }
}
