package com.game.circle;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

public class ArrayListSimulatorImplTest extends SimulatorTest {

    @BeforeEach
    public void init() {
        simulator = new ArrayListSimulatorImpl();
    }

    // Aborted. Takes more than 30 min and still running.
    @Disabled
    @Test
    public void testLargeNumberStepOneCompletes() {
        Object result = simulator.runSimulation(Integer.MAX_VALUE / 100, 1);
        assertNotNull(result);
    }
}
