package com.game.circle;

import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertNotNull;

abstract public class SimulatorTest {

    protected Simulator simulator;

    @Test
    public void testInvalidZeroArgumentsForN() {
        assertThrows(
                IllegalArgumentException.class,
                () -> simulator.runSimulation(0, 1)
        );
    }

    @Test
    public void testInvalidZeroArgumentsForK() {
        assertThrows(
                IllegalArgumentException.class,
                () -> simulator.runSimulation(1, 0)
        );
    }

    @Test
    public void testInvalidNegativeArgumentsForN() {
        assertThrows(
                IllegalArgumentException.class,
                () -> simulator.runSimulation(-1, 1)
        );
    }

    @Test
    public void testInvalidNegativeArgumentsForK() {
        assertThrows(
                IllegalArgumentException.class,
                () -> simulator.runSimulation(1, -1)
        );
    }

    @Test
    public void testSixStepOne() {
        Integer[] testSequence = new Integer[]{1, 2, 3, 4, 5, 6};
        Result result = simulator.runSimulation(6, 1);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }


    @Test
    public void testSixStepThree() {
        Integer[] testSequence = new Integer[]{3, 6, 4, 2, 5, 1};
        Result result = simulator.runSimulation(6, 3);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }


    @Test
    public void testTenStepFour() {
        Integer[] testSequence = new Integer[]{4, 8, 2, 7, 3, 10, 9, 1, 6, 5};
        Result result = simulator.runSimulation(10, 4);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }

    @Test
    public void testElevenStepFour() {
        Integer[] testSequence = new Integer[]{4, 8, 1, 6, 11, 7, 3, 2, 5, 10, 9};
        Result result = simulator.runSimulation(11, 4);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }

    @Test
    public void testElevenStepThree() {
        Integer[] testSequence = new Integer[]{3, 6, 9, 1, 5, 10, 4, 11, 8, 2, 7};
        Result result = simulator.runSimulation(11, 3);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }

    @Test
    public void testElevenStepSeven() {
        Integer[] testSequence = new Integer[]{7, 3, 11, 9, 8, 10, 2, 6, 1, 4, 5};
        Result result = simulator.runSimulation(11, 7);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }

    @Test
    public void testFortyStepSeven() {
        Integer[] testSequence = new Integer[]{7, 14, 21, 28, 35, 2, 10, 18, 26, 34, 3, 12, 22, 31, 40, 11, 23, 33, 5, 17,
                30, 4, 19, 36, 9, 27, 6, 25, 8, 32, 16, 1, 38, 37, 39, 15, 29, 13, 20, 24
        };

        Result result = simulator.runSimulation(40, 7);

        List<Integer> sequence = result.getEliminationSequence();
        sequence.add(result.getWinner());

        assertArrayEquals(testSequence, sequence.toArray(new Integer[0]));
    }


    @Test
    public void testThousandStepHundredCompletes() {
        Object result = simulator.runSimulation(1000, 100);
        assertNotNull(result);
    }

}